<ion-list>
    <ion-list-header>Emergency contacts you should know</ion-list-header>
    <ion-item>
      <ion-icon name="medkit" item-start></ion-icon>
      National Disaster Management Agency 
    </ion-item>
    <h1>1-473-440-8390</h1>

    <ion-item>
      <ion-icon name="medkit" item-start></ion-icon>
      Grenada General Hospital
    </ion-item>
    <h1>1 473-440-2051</h1>

    <ion-item>
      <ion-icon name="medkit" item-start></ion-icon>
      Grenada Coast Gaurds
    </ion-item>
    <h1>1 473-444-1931</h1>

    <ion-item>
      <ion-icon name="medkit" item-start></ion-icon>
      Grenada Police Force (Head Qauters)
    </ion-item>
    <h1>1 (473) 440-3999</h1>
  </ion-list>
  <ion-list>